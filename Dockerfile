FROM registry.gitlab.com/nanofabio/mfjupyterlab/jupyterlab-ex
# Get the latest image tag at:
# https://hub.docker.com/r/jupyter/datascience-notebook/tags/
# Inspect the Dockerfile at:
# https://hub.docker.com/r/jupyter/datascience-notebook/dockerfile

# install additional package...
# bio packages compiled from source or downloaded as binaries will live in the /bio directory
USER root
ENV BIO_DIR=/bio
ENV PATH=$BIO_DIR/bin:$PATH

RUN mkdir -p $BIO_DIR
RUN mkdir -p $BIO_DIR/bin

# install additional packages for prereqs
RUN apt-get update
RUN apt-get install -yq --no-install-recommends \
    asciinema \
    bc \
    build-essential \
    csh \
    curl \
    default-jdk \
    fuse3 \
    git \
    graphviz \
    hashdeep \
    htop \
    iproute2 \
    iputils-ping \
    iputils-tracepath \
    jq \
    less \
    libbz2-dev \
    libfuse2 \
    libgconf-2-4 \
    libgl1-mesa-glx \
    libgtk2.0-0 \
    liblzma-dev \
    libncurses5-dev \
    libssl-dev \
    libxrender1 \
    libxext6 \
    libxt6 \
    parallel \
    rename \
    rsync \
    screen \
    ssh \
    tmux \
    tree \
    unzip \
    vim \
    xvfb \
    zlib1g-dev \
    zip

RUN curl https://rclone.org/install.sh | bash

USER $NB_UID

# Here I'm just installing directly to the base conda installation
RUN conda install -c conda-forge mamba

RUN mamba install --yes \
    -c anaconda \
    -c bioconda \
    -c bokeh \
    -c cadquery \
    -c conda-forge \
    -c plotly \
    -c pyviz \
    bash_kernel \
    biopython \
    bokeh \
    bottleneck \
    colorlover \
    dash \
    dask \
    dask-labextension \
    datashader \
    distributed \
    gitpython \
    hdbscan \
    htseq \
    humanize \
    jupyterlab-git \
    jupyter-server-proxy \
    netCDF4 \
    opencv \
    papermill \
    plotly \
    plotly-orca \
    primer3-py \
    pyfuse3 \
    pytables \
    umap-learn \
    xarray \
    xmltodict \
    holoviews \
    hvplot \
    markdown \
    panel \
    pyviz_comms 
    
# the following either can't be found in conda or are severely out of date
RUN pip install \
  batchspawner \
  exif \
  graphviz \
  h5netcdf \
  imutils \
  jupyterthemes \
  opencv-python \
  ratarmount \
  sh 
  
RUN jt -t grade3 -cellw 80% -ofs 11 -dfs 10 -fs 11 -tfs 11 -nfs 11 -mathfs 11
